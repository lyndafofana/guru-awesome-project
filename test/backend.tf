terraform {
  backend "s3" {
    bucket         = "lynda-terrastate-bucket"
    key            = "test/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}
