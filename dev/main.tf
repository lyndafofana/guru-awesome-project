provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "this" {
  bucket = "${var.bucket_prefix}-${var.environment}"
#   acl    = "private"

  tags = {
    Environment = var.environment
  }
}
