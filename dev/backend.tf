terraform {
  backend "s3" {
    bucket         = "lynda-terrastate-bucket"
    key            = "dev/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}


# test 2