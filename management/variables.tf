variable "environment" {
  description = "The environment for the deployment"
  type        = string
}

variable "bucket_prefix" {
  description = "Prefix for the S3 bucket name"
  type        = string
}
