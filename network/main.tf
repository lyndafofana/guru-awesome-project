provider "aws" {
  region = "eu-west-1"
}

resource "aws_s3_bucket" "this" {
  bucket = "${var.bucket_prefix}-${var.environment}"

  tags = {
    Environment = var.environment
  }
}
