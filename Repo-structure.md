/
|-- dev/
|   |-- main.tf
|   |-- variables.tf
|   |-- outputs.tf
|-- prod/
|   |-- main.tf
|   |-- variables.tf
|   |-- outputs.tf
|-- test/
|   |-- main.tf
|   |-- variables.tf
|   |-- outputs.tf
|-- network/
|   |-- main.tf
|   |-- variables.tf
|   |-- outputs.tf
|-- management/
|   |-- main.tf
|   |-- variables.tf
|   |-- outputs.tf
|-- .gitlab-ci.yml
|-- .gitlab
|   |-- pipelines
|   |   |-- dev-pipeline
|   |   |-- test-pipeline
|   |   |-- prod-pipeline
|   |   |-- network-pipeline
|   |   |-- management-pipeline
